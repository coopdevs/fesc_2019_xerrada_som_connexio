title: Som Connexió
class: animation-fade
layout: true


<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  {{title}}, abordant el repte tecnològic de créixer.
]

---

class: impact

# {{title}}
## Abordant el repte tecnològic de créixer.

---

# Qui som?

- Joan Caballero (Som Connexió)
- Enrico Stano (Coopdevs)

---

# Singulars 2018-2019

### “Estudi, desenvolupament i acompanyament per la implementació de sistemes ERP de programari lliure especialitzats en cooperatives i ESS”

- Coopdevs
- Som Connexió
- FACTO

<br/>
<p align="right">
  <img src="images/logos_singulars.png" width="450">
</p>

---

# Cooperativisme obert

 - Transparència
 - Replicabilitat
 - Compartir processos estratègics i IT a nivell de sector
 - Projecció en l'àmbit internacional
 - Intercooperació

---

# Reptes de Som Connexió a inici de 2018

 - Processos manuals, les funcions normals requereixen molt esforç per part de l’equip
 - Sistemes no preparats per a la gestió de grans volums de dades
 - Forta dependència dels proveïdors de serveis
 - Molt baixa inversió inicial en sistemes: la cooperativa va començar fent les coses, els sistemes per donar suport a l’activitat van venir després
 - Forta dependència de Gsuite
 - Dificultat per trobar programadores amb experiència

---
<style>
  .centered { text-align: center; }
  .green-bg { background-color: #00773f }
  .red-bg { background-color: #ff79a5 }
  .yellow-bg { background-color: #9b9030 }
  .cyan-bg { background-color: #848484 }
  .pad { padding: 5px; }
</style>

# Funcions que fa actualment Som Connexió

<table style="font-size: 20px; width: 100%;">
  <tr>
    <th class="centered"></th>
    <th class="centered">OMR</th>
    <th class="centered">OMV Complert</th>
    <th class="centered">OMV prestador<br/>de serveis</th>
    <th class="centered">Revenedor</th>
    <th class="centered">Som Connexió</th>
  </tr>
  <tr>
    <td>Xarxa accés radio</td>
    <td class="centered green-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
  </tr>
  <tr>
    <td>Xarxa commutació</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
  </tr>
  <tr>
    <td>Interconnexió i roaming</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
  </tr>
  <tr>
    <td>Interceptació legal</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered red-bg"></td>
  </tr>
  <tr>
    <td>Provisió i pricing</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered yellow-bg"></td>
    <td class="centered red-bg"></td>
    <td class="centered cyan-bg"></td>
  </tr>
  <tr>
    <td>Gestió de terminals</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered yellow-bg"></td>
    <td class="centered yellow-bg"></td>
    <td class="centered red-bg"></td>
  </tr>
  <tr>
    <td>Gestió targetes SIM</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered yellow-bg"></td>
    <td class="centered yellow-bg"></td>
    <td class="centered green-bg"></td>
  </tr>
  <tr>
    <td>Facturació i gestió de cobraments</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
  </tr>
  <tr>
    <td>Atenció a la persona consumidora</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
  </tr>
  <tr>
    <td>Distribució</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
  </tr>
  <tr>
    <td>Vendes i Marketing</td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
    <td class="centered green-bg"></td>
  </tr>
</table>
<br/>
<table style="font-size: 15px">
  <tr>
    <td class="centered green-bg pad">Funció realitzada pel rol</td>
    <td class="centered red-bg pad">Funció NO realitzada pel rol</td>
    <td class="centered yellow-bg pad">Funció que el rol pot realitzar o no</td>
    <td class="centered cyan-bg pad">Funció realitzada parcialment</td>
  </tr>
</table>

---

# Mapa de sistemes

<br/>
<p align="center">
  <img src="images/SC_ida.png" width="400">
</p>

---

# Procés de provisió del servei:
## Sistema de gestió de processos (BPM)

S’escull una eina que ens permeti:
 - gestionar les sol·licituds de les consumidores (tickets)
 - automatizar les gestions i comunicacions de processos complexos com la provisió d’ADSL

La solució escollida és **OTRS**, de codi obert i amb la opció d’obtenir suport d’empreses especialitzades.

---

# Processos de facturació

<br/>
<p align="center">
  <img src="images/SC_abast_OC.png" width="650">
</p>

---

# Solució específica de sector

<p style="font-size: 15px" align="center">
  <img src="images/SC_solucio_especifica.png" width="400">
  <br/>
  <br/>
  Fuente: https://www.tutorialspoint.com/telecom-billing/billing-system-interfaces.htm
</p>

---

# Facturació per subscripció i ús
## Mediation
La **mediació** és el procés de **rebre registres d’ús** des de diferents fonts i adaptar-los al model de dades.

Fonts de fitxers:

 - Mòbil per al servei de mòbil (+1M registres mensuals)
 - Telefonia fixa, sobre ADSL o fibra (5000 registres mensuals)

Els proveïdors ens fan arribar els fitxers en format text. En el futur, quan adoptem un model de MVNO problablement rebrem cada registre d’ús en temps real via API.

---

# Facturació per subscripció i ús
## Rating 1
El procés de rating és l’acció d’**assignar un cost a cada registre** de trucada o connexió de dades.

En el moment que adoptem un model de MVNO, aquest procés de rating s’ha de fer en temps real, ja que el cost de cada trucada té impacte en l’estat del servei:

 1. Si el consum implica la superació del límit de consum sol·licitat, cal tallar el servei
 1. Si el servei es limitat, cal aplicar una baixada de velocitat o tall del servei
 1. Si hi ha un esquema de prepagament, també cal restringir les trucades si s’ha arribat a l’import assignat

---

# Facturació per subscripció i ús
## Rating 2

El procés de rating és **complex**, i tendeix a una complexitat cada cop major quan es va incrementant el catàleg de serveis oferts.

Amb un catàleg extremadament simple, com el nostre, cal comprovar el següent:
 - Tipus d’ús, per determinar si es factura fora d’abonament
 - Mantenir un comptador d’ús actual
 - Assignar preu depenent d’ús: establiment, minuts inclosos, preu per segon després dels inclosos
 - Serveis que es facturen pro-rata i serveis que es facturen a preu fix

---

# Facturació per subscripció i ús
## Rating 3
A mesura que s’inclouen nous serveis, augmenta la complexitat:

 - Centraletes virtuals per empresa: N línies que es facturen juntes, amb discriminació horària
 - Paquets de dades mòbils per a famílies, empreses (3GB compartits) amb diferents mètodes de pagament
 - Etc, etc.

Cal modelitzar productes complexos i estructures de persones / organitzacions complexes.

---

# Solució específica de sector
## Flexibilitat mitjançant configuració

<p align="center">
  <img src="images/SC_rating.png" width="650">
</p>

---

# System landscape Som Connexió abans del projecte

<p align="center">
  <img src="images/SC_landscape_abans.png" width="350">
</p>

---

# Criteris per la selecció de sistemes

 - Codi obert, possiblement lliure
 - Comunitats
 - Visió de futur
 - Solucions verticals

---

# ERP com a "single source of truth"

 - Evitar duplicació continguts
 - Assegurar consistència de dades
 - Facilitar processos de QA (test, qualitat, etc.)

---

# Integracions

 - Estudi i anàlisi de les APIs
 - Desenvolupament clients de les APIs
 - Estratègia desenvolupament amb visió de futur (abstraccions i middlewares)

---

# Equip del projecte

### Som Connexió
 - Analisis, estrategia i coordinacio (1 persona)
 - Suport, QA (1 persona)

### Coopdevs
 - Analisis, estrategia i coordinacio (1 persona)
 - Desenvolupament (2 persones)
 - Administracio de sistemes (1 persona)

---

# Següents passos

<p align="center">
  <img src="images/SC_landscape_despres.png" width="550">
</p>

---
class: impact

# Preguntes

---
class: impact

# Gràcies

<div style="display: table">
  <div style="position: absolute; width: 100%; display: table-cell; vertical-align: middle; text-align: center;">
    <div style="display: inline-block; padding: 20px;">
      <img src="images/SC_logo.png" width="150">
    </div>
    <div style="display: inline-block; padding: 20px;">
    <img src="images/logo_without_slogan.svg" width="150">
      </div>
    <div style="display: inline-block; padding: 20px;">
      <img src="images/FACTO_logo.svg" width="150">
    </div>
  </div>
</p>
